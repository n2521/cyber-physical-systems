package mqtt;

import runtime.IStateMachine;
import runtime.Scheduler;
import runtime.Timer;
import runtime.FreePool;

import sensehat.LEDMatrixTicker;


public class MQTTClientStateMachine implements IStateMachine {
	private MQTTclient client;
	private LEDMatrixTicker ticker;
        private FreePool fp;
	private String msg;
	private Timer tick_timer = new Timer("t");
	String EVT_RDY = "LEDMatrixReady",
		   EVT_ERR = "LEDMatrixError", 
		   EVT_WAIT = "LEDMatrixTickerWait", 
		   EVT_FIN = "LEDMatrixTickerFinished",
		   TIMER = "t";
	private enum STATES {INIT, S1, S2, S3, S4, S5, S6, FINAL};
	private STATES state = STATES.INIT;

        private boolean event_in_buffer = false;

	@Override
	public int fire(String event, Scheduler scheduler) {
            if (event.equals(MQTTclient.MSG_RCV)) {
                event_in_buffer = true;
            }

            if (state == STATES.INIT) {
                if (event.equals("Start")) {
                    client = new MQTTclient("tcp://broker.hivemq.com:1883", "oskar2", false, scheduler);
                    fp = new FreePool(0);
                    state = STATES.S1;
                    return EXECUTE_TRANSITION;
                }
            } else if (state == STATES.S1) {
                if (event.equals("MQTTError")) {
                    state = STATES.FINAL;
                    return TERMINATE_SYSTEM;
                } else if (event.equals("MQTTReady")) {
                    ticker = new LEDMatrixTicker(scheduler);
                    state = STATES.S2;
                    return EXECUTE_TRANSITION;
                }

            } else if (state == STATES.S2) {
                if (event.equals(MQTTclient.CONN_LOST) || event.equals(EVT_ERR)) {
                    state = STATES.FINAL;
                    return TERMINATE_SYSTEM;
                } else if (event.equals(EVT_RDY)) {
                    client.subscribe("/led_msg");
                    System.out.println("Subscribing to /led_msg");
                    state = STATES.S3;
                    return EXECUTE_TRANSITION;
                }

            } else if (state == STATES.S3) {
                if (event.equals(MQTTclient.CONN_LOST)) {
                    state = STATES.FINAL;
                    return TERMINATE_SYSTEM;
                } else if (event_in_buffer) {
                    event_in_buffer = false;
                    msg = fp.stripFreepool(client.getMessage());
                    System.out.println(msg);
                    ticker.StartWriting(msg);

                    state = STATES.S4;
                    return EXECUTE_TRANSITION;
                }
            } else if (state == STATES.S4) {
                if (event.equals(MQTTclient.CONN_LOST)) {
                    state = STATES.FINAL;
                    return TERMINATE_SYSTEM;
                } else if (event.equals(EVT_ERR)) {
                    state = STATES.S3;
                    return EXECUTE_TRANSITION;
                } else if (event.equals(EVT_WAIT)) {
                    // Here we start the first timer
                    tick_timer.start(scheduler, 100);
                    state = STATES.S5;
                    return EXECUTE_TRANSITION;
                }
            } else if (state == STATES.S5) {
                if (event.equals(MQTTclient.CONN_LOST) || event.equals(EVT_ERR)) {
                    state = STATES.FINAL;
                    return TERMINATE_SYSTEM;
                } else if (event.equals(TIMER)) {
                    // Here we handle the timer.
                    ticker.WritingStep();
                    state = STATES.S6;
                    return EXECUTE_TRANSITION;
                } else if (event.equals("LEDMatrixTickerFinished")) {
                    String msg = fp.createMessage("done");
                    if(msg != null) {
                        client.publishMessage("/led_msg_in", msg); 
                    } else {
                        System.err.println("Somehow out of freepools");
                    }
                    state = STATES.S3;
                    return EXECUTE_TRANSITION;
                }
            } else if (state == STATES.S6) {
                if (event.equals(MQTTclient.CONN_LOST) || event.equals(EVT_ERR)) {
                    state = STATES.FINAL;
                    return TERMINATE_SYSTEM;
                } else if (event.equals(EVT_WAIT)) {
                    // We wait for a message from the matrix
                    tick_timer.start(scheduler,  100);
                    state = STATES.S5; 
                    return EXECUTE_TRANSITION;
                } else if (event.equals("LEDMatrixTickerFinished")) {
                    String msg = fp.createMessage("done");
                    if(msg != null) {
                        client.publishMessage("/led_msg_in", msg); 
                    } else {
                        System.err.println("Somehow out of freepools");
                    }
                    if (event_in_buffer) {

                        // Since the scheduler will not fire the next transition uless
                        // it receives an event we just create a 'buffered' event.
                        scheduler.addToQueueLast(MQTTclient.MSG_RCV);
                    }
                    state = STATES.S3;
                    return EXECUTE_TRANSITION;
                }

            } else if (state == STATES.FINAL) {
                System.exit(0);
            }

            return DISCARD_EVENT;
        }

}
