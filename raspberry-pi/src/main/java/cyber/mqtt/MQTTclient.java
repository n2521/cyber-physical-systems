package mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;

import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.util.Queue;
import java.util.LinkedList;

import runtime.Scheduler;

public class MQTTclient implements MqttCallback {
	
	private Scheduler scheduler;
	private MqttClient client;
	
	public static final String CONN_LOST = "Connection Lost",
			MSG_DELIVERED = "Message delivered",
			MSG_RCV = "Message received";
	
	private Queue<String> queue;
	
	public MQTTclient(String broker, String myAddress, boolean conf, Scheduler s) {
		scheduler = s;
		queue = new LinkedList<String>();
		MemoryPersistence pers = new MemoryPersistence();
		try {
			client = new MqttClient(broker,myAddress,pers);
			MqttConnectOptions opts = new MqttConnectOptions();
			opts.setCleanSession(true);
			client.connect(opts);
			client.setCallback(this);
			scheduler.addToQueueLast("MQTTReady");
		}
		catch (MqttException e) {
			System.err.println("MQTT Exception: " + e);
			scheduler.addToQueueLast("MQTTError");
		}		
	}
	

	
	public void connectionLost(Throwable e) {
		scheduler.addToQueueLast(CONN_LOST);
	}
	
	public void deliveryComplete(IMqttDeliveryToken token) {
		scheduler.addToQueueLast(MSG_DELIVERED);
	}
	
	public void messageArrived(String topic, MqttMessage mess) {
		queue.add(mess.toString());
		scheduler.addToQueueLast(MSG_RCV);
	}
	
	public String getMessage() {
		return queue.remove();
	}
	
	public void publishMessage(String topic, String msg) {
		MqttMessage message = new MqttMessage(msg.getBytes());
		message.setQos(2);
		try {			
			client.publish(topic, message);
		} catch (MqttException e) {
			System.err.println("MQTT Send Exception: " + e);
		}
	}
	
	public void subscribe(String topic) {
		try {			
			client.subscribe(topic, 2);
		} catch (MqttException e) {
			System.err.println("MQTT Subscribe Exception: " + e);
		}
	}
	
}
