package cyber;

import runtime.StringInput;
import runtime.IStateMachine;
import runtime.Scheduler;
import mqtt.MQTTclient;
import runtime.FreePool;

public class Unit3CLI implements IStateMachine {

    private enum STATES {INIT, MQTT_INIT, READY};
    private STATES state =  STATES.INIT;
    private MQTTclient client;
    private FreePool fp;
    private StringInput si;

    public int fire(String event, Scheduler s) {
        if(state == STATES.INIT) {
            client = new MQTTclient("tcp://broker.hivemq.com:1883", "oskar_cli", false, s);
            fp = new FreePool(3);
            si = new StringInput(s);
            si.start();
            System.out.println("Waiting for MQTT");
            state = STATES.MQTT_INIT;
            return EXECUTE_TRANSITION;
        } else if(state == STATES.MQTT_INIT) {
            if (event.equals("MQTTError")) {
                return TERMINATE_SYSTEM;
            } else if (event.equals("MQTTReady")) {
                state = STATES.READY;
                client.subscribe("/led_msg_in");
                si.call();
                return EXECUTE_TRANSITION;
            }
        } else if (state == STATES.READY) {
            if (event.equals("ReaderInput")) {
                String msg = fp.createMessage(si.getInput()); 
                if(msg != null) {
                    client.publishMessage("/led_msg", msg);
                }
                if (fp.freepoolAvailable()) {
                    System.out.println("Calling thread");
                    si.call();
                } else {

                }
            } else if (event.equals(MQTTclient.MSG_RCV)) {
                String msg = fp.stripFreepool(client.getMessage());
                System.out.println(msg);
                si.call();
            }
            return EXECUTE_TRANSITION;
        }
        return DISCARD_EVENT;
    }

    public static void main(String[] args) {
        IStateMachine stm = new Unit3CLI();
        Scheduler s = new Scheduler(stm);
        System.out.println("Running scheduler");
        s.addToQueueFirst("Start");
        s.run();
    }
}

