package exercises.trafficlight;

import runtime.IStateMachine;
import runtime.Scheduler;
import runtime.Timer;


public class SynchronizerMachine implements IStateMachine {

	private IStateMachine stm1 = new TrafficLightControllerMachine();
	private IStateMachine stm2 = new TrafficLightControllerMachine();
	private IStateMachine stm3 = new TrafficLightControllerMachine();
	
	private Scheduler s1 = new Scheduler(stm1);
	private Scheduler s2 = new Scheduler(stm2);
	private Scheduler s3 = new Scheduler(stm3);
	
	private Timer t1 = new Timer("t1");
	private Timer t2 = new Timer("t2");
	private final String T1 = "t1", T2 = "t2";
	
	@Override
	public int fire(String event, Scheduler scheduler) {
		
		if (event.equals(T1)) {
			t1.start(scheduler, 60000);
			s1.addToQueueFirst("t60");
			s2.addToQueueFirst("t60");
			t2.start(scheduler, 30000);
			return EXECUTE_TRANSITION;

		} else if (event.equals(T2)) {
			return EXECUTE_TRANSITION;
		}
		
		return DISCARD_EVENT;
	}
	
	public SynchronizerMachine() {
		s1.start();
		s2.start();
		s3.start();
		
		
	}
	
	public void main() {
		IStateMachine stm = new SynchronizerMachine();
		Scheduler sched = new Scheduler(stm);
		
		sched.start();
	}
	
	
	
}
