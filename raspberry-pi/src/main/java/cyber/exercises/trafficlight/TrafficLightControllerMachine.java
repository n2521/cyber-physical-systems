package exercises.trafficlight;

import runtime.EventWindow;
import runtime.IStateMachine;
import runtime.Scheduler;
import runtime.Timer;


public class TrafficLightControllerMachine implements IStateMachine {
	
	private static final String PEDESTRIAN_BUTTON_PRESSED = "Pedestrian Button", T1 = "t1", T2 = "t2", T3 = "t3", T4 = "t4", T5 = "t5", T6 = "t6";
	
	public static final String[] EVENTS = {PEDESTRIAN_BUTTON_PRESSED};
	
	private enum STATES {S0, S1, S2, S3, S4, S5}
	
	private Timer t1 = new Timer("t1");
	private Timer t2 = new Timer("t2");
	private Timer t3 = new Timer("t3");
	private Timer t4 = new Timer("t4");
	private Timer t5 = new Timer("t5");
	private Timer t6 = new Timer("t6");
	
	
	private boolean button_push = false;
	private boolean cars_run = true;
	
	protected STATES state = STATES.S0;
	
	private TrafficLightController cars = new TrafficLightController("Cars", false);
	private TrafficLightController pedestrians = new TrafficLightController("Pedestrians", true);
	
	public TrafficLightControllerMachine() {
		// initial transition

		cars.showGreen();
		pedestrians.showRed(false);		
	}

	public int fire(String event, Scheduler scheduler) {
		if(event.equals(PEDESTRIAN_BUTTON_PRESSED)) {
			button_push = true;
		}
		if(state==STATES.S0) {
			if(event.equals(T6)) {
				cars_run = true;
			}
			if(button_push && cars_run) {
				button_push = false;
				cars_run = false;
				cars.showYellow();
				t1.start(scheduler, 1000);
				state = STATES.S1;
				return EXECUTE_TRANSITION;
			}  
		} else if(state==STATES.S1) {
			if(event.equals(T1)) {
				cars.showRed(false);
				t2.start(scheduler, 1000);
				state = STATES.S2;
				return EXECUTE_TRANSITION;
			}
		} else if(state == STATES.S2) {
			if(event.equals(T2)) {
				pedestrians.showGreen();
				t3.start(scheduler, 1000);
				state = STATES.S3;
				return EXECUTE_TRANSITION;
			}
		} else if(state == STATES.S3) {
			if(event.equals(T3)) {
				pedestrians.showRed(false);
				t4.start(scheduler, 1000);
				state = STATES.S4;
				return EXECUTE_TRANSITION;
			}
			
		} else if(state == STATES.S4) {
			if(event.equals(T4)) {
				cars.showRed(true);
				t5.start(scheduler, 1000);
				state = STATES.S5;
				return EXECUTE_TRANSITION;
			}
		} else if(state == STATES.S5) {
			if(event.equals(T5)) {
				cars.showGreen();
				state = STATES.S0;
				t6.start(scheduler, 20000);
				return EXECUTE_TRANSITION;
			}
		} 
		return DISCARD_EVENT;
	}
}
