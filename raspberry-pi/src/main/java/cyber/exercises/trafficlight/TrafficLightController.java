package exercises.trafficlight;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.Pin;

public class TrafficLightController {

	private static final Pin RED_1 = RaspiPin.GPIO_00,
					  RED_2 = RaspiPin.GPIO_07,
					  YELLOW_1 = RaspiPin.GPIO_03,
					  YELLOW_2 = RaspiPin.GPIO_12,
					  GREEN_1 = RaspiPin.GPIO_13,
					  GREEN_2 = RaspiPin.GPIO_14;

	private GpioPinDigitalOutput red_pin;
	private GpioPinDigitalOutput yellow_pin;
	private GpioPinDigitalOutput green_pin;
	
	public TrafficLightController(String name, boolean pedestrian) {
		final GpioController gpio = GpioFactory.getInstance();
		if (pedestrian) {
			red_pin = gpio.provisionDigitalOutputPin(TrafficLightController.RED_1, "RED1", PinState.HIGH);
			yellow_pin = gpio.provisionDigitalOutputPin(TrafficLightController.YELLOW_1, "Y1", PinState.LOW);
			green_pin = gpio.provisionDigitalOutputPin(TrafficLightController.GREEN_1, "G1", PinState.LOW);
		} else {
			red_pin = gpio.provisionDigitalOutputPin(TrafficLightController.RED_2, "RED2", PinState.HIGH);
			yellow_pin = gpio.provisionDigitalOutputPin(TrafficLightController.YELLOW_2, "Y2", PinState.LOW);
			green_pin = gpio.provisionDigitalOutputPin(TrafficLightController.GREEN_2, "G2", PinState.LOW);	
		}
		
		
		
	}
	
	public void showGreen() {
		green_pin.high();
		red_pin.low();
		yellow_pin.low();
	}
	
	public void showYellow() {
		green_pin.low();
		yellow_pin.high();
		red_pin.low();
		
	}
	
	public void showRed(boolean withYellow) {
		green_pin.low();

		if (withYellow) {
			yellow_pin.high();

		} else {
			yellow_pin.low();

		}
		
		red_pin.high();
		
	}

}

