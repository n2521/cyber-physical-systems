package cyber;

import mqtt.MQTTClientStateMachine;
import runtime.Scheduler;

public class Unit3 {
    public static void main(String[] args) {
        System.out.println("Initializing MQTT Service");
        MQTTClientStateMachine stm = new MQTTClientStateMachine();

        System.out.println("Initializing Scheduler");
        Scheduler s = new Scheduler(stm);

        System.out.println("Running system");
        s.addToQueueFirst("Start");
        s.run();
    }

}
