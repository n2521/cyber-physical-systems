package runtime;

import java.lang.Thread;
import java.util.Queue;
import java.util.LinkedList;


public class FreePool extends Thread {

    private Queue<String> freePoolQueue;
    private Queue<String> messageQueue;

    public FreePool(int nPools) {
        freePoolQueue = new LinkedList();
        messageQueue = new LinkedList();
        for(int i = 0; i < nPools; ++i) {
            freePoolQueue.add("*");
        }
    }

    public String stripFreepool(String msg) {
        if(msg.startsWith("*")) {
            freePoolQueue.add("*");
            return msg.substring(1);
        }

        return msg;
    }

    public String createMessage(String msg) {
        messageQueue.add(msg);

        if(freePoolQueue.peek() == null) {
            // If there are no freepools left we just store the message
            return null;
        }

        // There are freepools left so we can return the message at the front of the queue
        return freePoolQueue.remove() + messageQueue.remove();
    }

    public boolean freepoolAvailable() {
        return freePoolQueue.size() != 0;
    }
    
    public boolean messagesAvailable() {
        return messageQueue.size() != 0;
    }

    public boolean readyToSend() {
        return freepoolAvailable() && messagesAvailable(); 
    }

}
