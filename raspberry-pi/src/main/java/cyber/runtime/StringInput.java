package runtime;

import java.lang.Thread;
import java.util.concurrent.*;
import java.util.Scanner;

public class StringInput extends Thread {

    private Scheduler scheduler;
    private Semaphore sem;
    private String inputString;
    private Scanner userin;
    private boolean running;

    public StringInput(Scheduler s) {
        scheduler = s;
        sem = new Semaphore(0);
        userin = new Scanner(System.in);
    }

    public synchronized void call() {
        sem.release();			
    }

    public synchronized String getInput() {
        return inputString;
    }

    public synchronized void terminate() {
        running = false;
        sem.release();
    }

    public void run() {
        try {
            running = true;
            while (running) {
                sem.acquire();
                if (running) {
                    System.out.print("Write a string: ");
                    inputString = userin.nextLine();
                    scheduler.addToQueueLast("ReaderInput");					
                }
            }
        } catch (InterruptedException e) {
            System.out.println("Exception caught when trying to acquire a semaphore slot:");
            System.out.println(e.getMessage());
        }
    }
}
