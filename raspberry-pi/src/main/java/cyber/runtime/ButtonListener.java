package runtime;


import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.io.gpio.Pin;

public class ButtonListener {
	
	private GpioPinDigitalInput pedestrianButton;
	final static Pin PED_BUTTON_PIN = RaspiPin.GPIO_11;
	private final Scheduler scheduler;
	
	public ButtonListener(Scheduler scheduler) {
		final GpioController gpio = GpioFactory.getInstance();
		this.scheduler = scheduler;
		
		pedestrianButton = gpio.provisionDigitalInputPin(ButtonListener.PED_BUTTON_PIN, PinPullResistance.PULL_DOWN);
		pedestrianButton.setShutdownOptions(true);
		
		pedestrianButton.addListener(new GpioPinListenerDigital() {
			@Override
			public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
				addBtnEvent();
			}
		});
		
	}
	
	private void addBtnEvent() {
		this.scheduler.addToQueueLast("Pedestrian Button");
	}
}
