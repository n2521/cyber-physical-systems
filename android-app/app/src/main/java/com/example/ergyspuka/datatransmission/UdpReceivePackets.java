package com.example.ergyspuka.datatransmission;

import android.widget.Toast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

/**
 * Created by Peter on 25.09.2017.
 */

public class UdpReceivePackets extends Thread {

    public DatagramSocket datagramSocket = null;
    UdpClient uct = null;
    boolean active;
    String received;

    Long rcvUdpPacketTime = null;
    Long sendUdpPacketTime = null;


    public UdpReceivePackets(DatagramSocket datagramSocket, UdpClient uct) {

        this.datagramSocket = datagramSocket;
        this.uct = uct;

    }

    public void setActive(boolean b) {
        this.active = b;
    }


    public void run() {
        System.out.println("UdpReceivePacketsManager Name: " + this.getName() + ", id: " + this.getId());

        while (uct.getUdpActive()) {
            byte[] buffer = new byte[256];
            DatagramPacket p = new DatagramPacket(buffer, buffer.length);

            try {
                datagramSocket.setSoTimeout(1000);
            } catch (SocketException e) {
                e.printStackTrace();
                continue;
            }

            try {
                datagramSocket.receive(p);
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }

            String s = new String(p.getData());
            System.out.println(s);
            long ts = Long.decode(s.substring(0, s.indexOf(';')));
            long round_trip = System.currentTimeMillis() - ts;
            uct.setRTD(round_trip);
            System.out.println("Round trip time: " + String.valueOf(round_trip));
        }
    }

}
