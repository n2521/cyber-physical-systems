/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package udp;

import udp.runtime.IStateMachineData;
import udp.runtime.SchedulerData;
import udp.runtime.UdpClient;

import udp.datastorage.*;

import java.net.DatagramPacket;
import java.io.File;

public class App implements IStateMachineData{

    static enum STATE {INIT, FILE_NAME, CONNECTING, WAITING, ECHO, FINAL};

    private STATE state = STATE.INIT;

    private UdpClient client;
    
    final private int PORT = 4422;

    private ChooseFileName cf;
    private WriteFile fh;

    @Override
    public int fire(String eventId, Object data, SchedulerData s) {
        if (state == STATE.INIT) {
            cf = new ChooseFileName(true, s);

            state = STATE.FILE_NAME;
            return EXECUTE_TRANSITION;
         
        } else if (state == STATE.FILE_NAME){
            if (eventId.equals("ChooseFilenameSave")) {
                fh = new WriteFile((File)data, s);
                fh.writeStringToFile("time_stamp;communication_technology;latitude;longitude;signal_str;rtd\r\n");

                client = new UdpClient(s, PORT);
                state = STATE.CONNECTING;
                return EXECUTE_TRANSITION;

            } else if(eventId.equals("ChooseFilenameCancel")) {
                
                return TERMINATE_SYSTEM;
            }

        } else if (state == STATE.CONNECTING) {
            if (eventId.equals(UdpClient.RDY)) {
                state = STATE.WAITING;
                return EXECUTE_TRANSITION;
            } else if(eventId.equals(UdpClient.BND_ERR)) {
                state = STATE.FINAL;
                return TERMINATE_SYSTEM;
            }
        } else if(state == STATE.WAITING) {
            if (eventId.equals(UdpClient.MSG_RCV)) {
                DatagramPacket d = (DatagramPacket)data;
                System.out.println("Size of data: " + String.valueOf(d.getLength()));
                String str = new String(d.getData());
                if (str.contains("EXIT")) {
                    fh.closeFile();
                    return TERMINATE_SYSTEM;
                }
                str = str.substring(0, d.getLength());
                fh.writeStringToFile(str+"\r\n");
                System.out.println(str);

                client.sendMessage(str.substring(0, d.getLength()), d.getAddress(), d.getPort());

                state = STATE.ECHO;
                return EXECUTE_TRANSITION;
            } else if(eventId.equals(UdpClient.RCV_ERR)) {
                state = STATE.FINAL;
                client.stop();
                return TERMINATE_SYSTEM;
            }
        } else if (state == STATE.ECHO) {
            if(eventId.equals(UdpClient.MSG_SENT)) {
                state = STATE.WAITING;
                return EXECUTE_TRANSITION;
            }
        }

        return DISCARD_EVENT;
    }

    public static void main(String[] args) {
        
        System.out.println("Creating FSM");
        IStateMachineData fsm = new App();

        System.out.println("Creating scheduler");
        SchedulerData s = new SchedulerData(fsm);

        System.out.println("Running scheduler");
        s.addToQueueLast("Start");
        s.run();

    }
}
