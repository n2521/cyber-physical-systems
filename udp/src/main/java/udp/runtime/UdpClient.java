package udp.runtime;

import udp.runtime.SchedulerData;
import java.lang.Runnable;
import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.InetAddress;


public class UdpClient {
    private DatagramSocket socket;

    private Thread th;

    private SchedulerData s;

    public static String SEND_ERR = "SocketSendError",
                  RCV_ERR = "SocketReceiveError",
                  MSG_SENT = "SocketMessageSent",
                  BND_ERR = "SocketBindError",
                  MSG_RCV = "SocketMessageReceived",
                  RDY = "SocketReady";

    public void stop() {
        th = null;
        socket.close();
    }

    public void sendMessage(String msg, InetAddress addr, int port) {
        byte[] strbuf = new byte[256];
        strbuf = msg.getBytes();
        System.out.println("Sending " + msg);
        System.out.println(msg.length());
        DatagramPacket packet = new DatagramPacket(strbuf, msg.length(), addr, port);
        
        try {
            this.socket.send(packet);
        } catch(Exception e) {
            System.out.println(e);
            s.addToQueueLast(SEND_ERR);
            return;
        }

        s.addToQueueLast(MSG_SENT);
        
    }

    public UdpClient(SchedulerData s, int port) {
        this.s = s;
        
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    socket = new DatagramSocket(port);
                } catch(Exception e) {
                    System.out.println(e);
                    s.addToQueueLast(BND_ERR);
                    return;
                }
                s.addToQueueLast(RDY);

                byte[] buffer;
                DatagramPacket packet;
                while(true) {
                    buffer = new byte[256];
                    packet = new DatagramPacket(buffer, buffer.length);

                    try {
                        socket.receive(packet);
                    } catch(Exception e) {
                        System.out.println(e);
                        s.addToQueueLast(RCV_ERR);
                        continue;
                    }

                    s.addToQueueLast(MSG_RCV, packet);

                }
            }
        };

        th = new Thread(r);
        th.start();

    }
}
